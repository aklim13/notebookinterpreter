# **Notebook Interpreter (Python)**

Evaluate python and javascript code using Spring boot REST API.

# Pre-requisite
* Downlaod Graalvm and install python using Gu (https://github.com/graalvm/graalvm-ce-builds/releases)
* Maven latest version
* Install postman to consume API

# Installation
* Open a terminal
* use `git clone https://gitlab.com/aklim13/notebookinterpreter`
* move inside the project using cd notebookinterpreter
* run project using `mvn spring-boot:run`
* From postman, you can consume this API. (URL by default is localhost:8080)

# Usage

## Swagger
* Open the url : http://localhost:8080/api/swagger-ui.html to check swagger page.

## API
* Endpoint /execute using POST to execute a piece of javascript or python code.
* Endpoint /languages using GET to retrieve the list of all supported languages (beware the language should be installed first on the machine) 

## Session Support
For the first request, the user sends a valid code to execute, the api will return a sessionId to be used later to access same variables.

First Request: 

``{
"code": "%python a=100",
"sessionId": ""
}``

Result will be : 

``{
"result": "",
"sessionId": "ef3d9c0e-1579-4d26-9887-0c0b3b9851ca"
}``

Second Request :

``{
"code": "%python print(a)",
"sessionId": "ef3d9c0e-1579-4d26-9887-0c0b3b9851ca"
}``

Result will be :

``{
"result": "100",
"sessionId": "ef3d9c0e-1579-4d26-9887-0c0b3b9851ca"
}``

User with different sessionId, access to different variable.

## Cleanup operation
Each hour, Spring boot clean all session that were not acessed since 2 hours ago to avoid having unused sessions.
