package com.pyinterpreter.notebook.services;

import com.pyinterpreter.notebook.v1.dto.NotebookRequest;
import com.pyinterpreter.notebook.v1.dto.NotebookResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class NotebookServiceImplTest {

    @Autowired
    private NotebookService notebookService;

    @Test
    public void testNormalCase() {
        NotebookResponse notebookResponse = notebookService.executeCode(
                new NotebookRequest("%python print(1+1);", "session1")
        );
        assertEquals("2", notebookResponse.getResult());
    }

    @Test
    public void testDeclaredVariableCase() {
        NotebookResponse notebookResponse = notebookService.executeCode(
                new NotebookRequest("%python a=1;", "session1")
        );
        assertTrue(notebookResponse.getResult().isEmpty());
    }

    @Test
    public void testSubsequentExecutionCase() {
        NotebookResponse response1 = notebookService.executeCode(
                new NotebookRequest("%js function print(){ console.log ('hello world');}", "session1")
        );
        assertTrue(response1.getResult().isEmpty());

        NotebookResponse response2 = notebookService.executeCode(
                new NotebookRequest("%js print();", response1.getSessionId())
        );
        assertEquals("hello world", response2.getResult());
    }
}
