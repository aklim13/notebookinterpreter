package com.pyinterpreter.notebook.v1.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.pyinterpreter.notebook.v1.dto.NotebookRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
public class NotebookControllerTest {
    private static final String NOTEBOOK_EXECUTE_ENDPOINT = "/v1/execute";
    private static final String NOTEBOOK_LANGUAGE_ENDPOINT = "/v1/languages";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void WhenValidRequestSentReturnResult() throws Exception {
        NotebookRequest notebookRequest = new NotebookRequest("%python print(1+1)", "");

        mockMvc.perform(
                post(NOTEBOOK_EXECUTE_ENDPOINT)
                        .content(mapper.writeValueAsString(notebookRequest))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isCreated())
                .andExpect(jsonPath("$.result", is("2")));
    }

    @Test
    public void WhenFVariableIsInitializedUsingSessionIdVariableIsAccessible() throws Exception {
        NotebookRequest notebookRequest = new NotebookRequest("%python a=100", "");

        String result = mockMvc.perform(
                post(NOTEBOOK_EXECUTE_ENDPOINT)
                        .content(mapper.writeValueAsString(notebookRequest))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isCreated())
                .andExpect(jsonPath("$.result", is("")))
                .andReturn()
                .getResponse()
                .getContentAsString();

        NotebookRequest notebookRequest2 = new NotebookRequest("%python print(a);", JsonPath.parse(result).read("$.sessionId"));
        mockMvc.perform(
                post(NOTEBOOK_EXECUTE_ENDPOINT)
                        .content(mapper.writeValueAsString(notebookRequest2))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isCreated())
                .andExpect(jsonPath("$.result", is("100")));
    }

    @Test
    public void WhenMultipleUsersSendRequestTheyAccessDifferentVariables() throws Exception {
        NotebookRequest userRequest1 = new NotebookRequest("%python a=2", "");

        String firstUserResult = mockMvc.perform(
                post(NOTEBOOK_EXECUTE_ENDPOINT)
                        .content(mapper.writeValueAsString(userRequest1))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isCreated())
                .andExpect(jsonPath("$.result", is("")))
                .andReturn()
                .getResponse()
                .getContentAsString();

        NotebookRequest userRequest2 = new NotebookRequest("%python print(a);", JsonPath.parse(firstUserResult).read("$.sessionId"));
        mockMvc.perform(
                post(NOTEBOOK_EXECUTE_ENDPOINT)
                        .content(mapper.writeValueAsString(userRequest2))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isCreated())
                .andExpect(jsonPath("$.result", is("2")));

        NotebookRequest secondUserRequest = new NotebookRequest("%python a=4", "");
        String secondUserResult = mockMvc.perform(
                post(NOTEBOOK_EXECUTE_ENDPOINT)
                        .content(mapper.writeValueAsString(secondUserRequest))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isCreated())
                .andExpect(jsonPath("$.result", is("")))
                .andReturn()
                .getResponse()
                .getContentAsString();

        NotebookRequest user2Request2 = new NotebookRequest("%python print(a);", JsonPath.parse(secondUserResult).read("$.sessionId"));
        mockMvc.perform(
                post(NOTEBOOK_EXECUTE_ENDPOINT)
                        .content(mapper.writeValueAsString(user2Request2))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isCreated())
                .andExpect(jsonPath("$.result", is("4")));

    }

    @Test
    public void whenRequestWithUnkownLanguageThrowException() throws Exception {
        mockMvc.perform(
                post(NOTEBOOK_EXECUTE_ENDPOINT)
                        .content(mapper.writeValueAsString(new NotebookRequest("%py test=2.5", "")))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("The language used py is not currently supported.")))
                .andExpect(jsonPath("$.code", is(400)));

    }

    @Test
    public void whenValidRequestSendToGetLanguagesReturnResult() throws Exception {
        mockMvc.perform(
                get(NOTEBOOK_LANGUAGE_ENDPOINT)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].language", is("python")))
                .andExpect(jsonPath("$[0].abbreviation", is("python")))
                .andExpect(jsonPath("$[1].abbreviation", is("js")))
                .andExpect(jsonPath("$[1].language", is("javascript")))
                .andExpect(jsonPath("$").isNotEmpty());
    }

}
