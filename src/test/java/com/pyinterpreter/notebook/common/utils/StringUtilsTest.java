package com.pyinterpreter.notebook.common.utils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static com.pyinterpreter.notebook.common.utils.StringUtils.extractCode;
import static com.pyinterpreter.notebook.common.utils.StringUtils.extractLanguage;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class StringUtilsTest {
    @Before
    public void setup() {

    }

    @Test
    public void checkLanguageExtractedWithoutPourcent() {
        assertEquals("python", extractLanguage(false, "%python 4"));
        assertEquals("js", extractLanguage(false, "%js 4"));
    }

    @Test
    public void checkLanguageExtractedWithPourcent() {
        assertEquals("%python ", extractLanguage(true, "%python 4"));
        assertEquals("%js ", extractLanguage(true, "%js 4"));
    }

    @Test
    public void checkCodeExtracted() {
        assertEquals(" 4+1", extractCode("%python 4+1"));
        assertEquals(" 4+1 \n 5+9 \n 55", extractCode("%js 4+1 \n 5+9 \n 55"));
    }
}
