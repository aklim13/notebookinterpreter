package com.pyinterpreter.notebook.common.validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class VerifyCodeValidatorTest {
    @Mock
    private VerifyCodeValidator verifyCodeValidator;

    @Before
    public void setup() {
        Mockito.when(verifyCodeValidator.isCodeValid(anyString())).thenCallRealMethod();
    }

    @Test
    public void whenCodeIsValidReturnTrue() {
        assertTrue(isCodeValid("%python 1+1"));
        assertTrue(isCodeValid("%python print(a);"));
        assertTrue(isCodeValid("%js console.log(1);"));
        assertTrue(isCodeValid("%js console.log(1);\n console.log(10);"));
        assertTrue(isCodeValid("%python a=1; print(a);"));
        assertTrue(isCodeValid("%jsssss console.log(1);"));

    }

    @Test
    public void whenCodeInvalidReturnFalse() {
        assertFalse(isCodeValid("%python"));
        assertFalse(isCodeValid("%%python"));
        assertFalse(isCodeValid("python print(a);"));
        assertFalse(isCodeValid("pytho print(a);"));
    }

    public boolean isCodeValid(String code) {
        return verifyCodeValidator.isCodeValid(code);
    }
}