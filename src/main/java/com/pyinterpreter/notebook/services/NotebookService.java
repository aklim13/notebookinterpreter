package com.pyinterpreter.notebook.services;

import com.pyinterpreter.notebook.v1.dto.NotebookRequest;
import com.pyinterpreter.notebook.v1.dto.NotebookResponse;
import com.pyinterpreter.notebook.v1.dto.SupportedLanguageResponse;

import java.util.List;

public interface NotebookService {
    /**
     * service that use graalvm context to execute code
     *
     * @param notebookRequest represent the code sent by the user
     * @return code result
     */
    NotebookResponse executeCode(NotebookRequest notebookRequest);

    /**
     * Get the list of all supported languages
     * Ideally, we should retrieve them from database
     *
     * @return list of supported languages
     */
    List<SupportedLanguageResponse> getSupportedLanguages();

}
