package com.pyinterpreter.notebook.services.impl;

import com.pyinterpreter.notebook.common.exceptions.CodeInterpreterException;
import com.pyinterpreter.notebook.common.exceptions.LanguageNotSupportedException;
import com.pyinterpreter.notebook.common.utils.StringUtils;
import com.pyinterpreter.notebook.services.NotebookService;
import com.pyinterpreter.notebook.v1.dto.CodeInterpreter;
import com.pyinterpreter.notebook.v1.dto.NotebookRequest;
import com.pyinterpreter.notebook.v1.dto.NotebookResponse;
import com.pyinterpreter.notebook.v1.dto.SupportedLanguageResponse;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.PolyglotException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

@Service
public class NotebookServiceImpl implements NotebookService {
    private static final Logger logging = LoggerFactory.getLogger(NotebookServiceImpl.class);

    private Map<String, CodeInterpreter> sessions;

    @Value("${interpreter.timeout}")
    private int timeout;

    @Autowired
    NotebookServiceImpl(Map<String, CodeInterpreter> sessions) {
        this.sessions = sessions;
    }

    @Override
    public NotebookResponse executeCode(NotebookRequest notebookRequest) {
        // extract code
        String code = StringUtils.extractCode(notebookRequest.getCode());

        //extract language used
        String language = StringUtils.extractLanguage(false, notebookRequest.getCode());

        // initialize a code interpreter
        CodeInterpreter codeInterpreter = getCodeInterpreter(notebookRequest);

        // run as a daemon
        Timer timer = new Timer(true);

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                logging.info("timer triggered closing context and remove it from session.");
                codeInterpreter.getContext().close(true);
                sessions.remove(notebookRequest.getSessionId());
            }
        }, timeout);

        try {
            // evaluate code sent by user
            codeInterpreter.getContext().eval(language, code);

            // read result from output stream and remove \n at the end of the result
            String result = codeInterpreter.getResult();

            return new NotebookResponse(notebookRequest.getSessionId(), result);
        } catch (PolyglotException e) {
            throw new CodeInterpreterException(e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new LanguageNotSupportedException(String.format("The language used %s is not currently supported.", language));
        } finally {
            // update last time access
            codeInterpreter.setUpdatedAt(new Date());

            // update session with new codeInterpreter
            sessions.put(notebookRequest.getSessionId(), codeInterpreter);

            // cancel timer if everything passed correctly
            timer.cancel();
        }
    }

    @Override
    public List<SupportedLanguageResponse> getSupportedLanguages() {
        // hardcoded list as we don't use database for this application.
        return Arrays.asList(
                new SupportedLanguageResponse("python", "python"),
                new SupportedLanguageResponse("javascript", "js")
        );
    }

    private CodeInterpreter getCodeInterpreter(NotebookRequest notebookRequest) {
        CodeInterpreter codeInterpreter;

        if (sessions.containsKey(notebookRequest.getSessionId())) {
            logging.info("session already exists");
            // retrieve code Interpreter from session
            codeInterpreter = sessions.get(notebookRequest.getSessionId());
        } else {
            logging.info("new session will be created");

            // read context result from output stream
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            // initialize graalvm context to interpret code
            Context context = Context.newBuilder()
                    .out(outputStream)
                    .build();

            // create a new code interpreter
            codeInterpreter = new CodeInterpreter(context, outputStream, new Date());

            // set new random sessionId
            notebookRequest.setSessionId(UUID.randomUUID().toString());

            // store codeInterpreter into session map
            sessions.put(notebookRequest.getSessionId(), codeInterpreter);
        }

        return codeInterpreter;
    }

}
