package com.pyinterpreter.notebook.v1.web;

import com.pyinterpreter.notebook.services.NotebookService;
import com.pyinterpreter.notebook.v1.api.NotebookApi;
import com.pyinterpreter.notebook.v1.dto.NotebookRequest;
import com.pyinterpreter.notebook.v1.dto.NotebookResponse;
import com.pyinterpreter.notebook.v1.dto.SupportedLanguageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class NotebookController implements NotebookApi {
    private NotebookService notebookService;

    @Autowired
    public NotebookController(NotebookService notebookService) {
        this.notebookService = notebookService;
    }

    public ResponseEntity<NotebookResponse> execute(@Valid @RequestBody NotebookRequest notebookRequest) {
        return new ResponseEntity<>(notebookService.executeCode(notebookRequest), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<SupportedLanguageResponse>> getSupportedLanguages() {
        return ResponseEntity.ok(notebookService.getSupportedLanguages());
    }
}
