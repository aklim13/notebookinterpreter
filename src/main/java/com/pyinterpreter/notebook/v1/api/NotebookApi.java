package com.pyinterpreter.notebook.v1.api;

import com.pyinterpreter.notebook.v1.dto.NotebookRequest;
import com.pyinterpreter.notebook.v1.dto.NotebookResponse;
import com.pyinterpreter.notebook.v1.dto.SupportedLanguageResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/v1")
public interface NotebookApi {
    /**
     * endpoint to execute code of any supported language
     *
     * @param notebookRequest dto that contains the code
     * @return result of executing the code
     */
    @PostMapping("/execute")
    ResponseEntity<NotebookResponse> execute(@Valid @RequestBody NotebookRequest notebookRequest);

    /**
     * endpoint to show supported languages to be used with this notebook
     *
     * @return list of all supported languages with their abreviation
     */
    @GetMapping("/languages")
    ResponseEntity<List<SupportedLanguageResponse>> getSupportedLanguages();
}
