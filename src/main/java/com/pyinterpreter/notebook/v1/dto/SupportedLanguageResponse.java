package com.pyinterpreter.notebook.v1.dto;

import java.util.Objects;

public class SupportedLanguageResponse {
    private String language;
    private String abbreviation;

    public SupportedLanguageResponse(String language, String abbreviation) {
        this.language = language;
        this.abbreviation = abbreviation;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SupportedLanguageResponse that = (SupportedLanguageResponse) o;
        return Objects.equals(language, that.language) &&
                Objects.equals(abbreviation, that.abbreviation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(language, abbreviation);
    }

    @Override
    public String toString() {
        return "SupportedLanguageResponse{" +
                "language='" + language + '\'' +
                ", abreviation='" + abbreviation + '\'' +
                '}';
    }
}
