package com.pyinterpreter.notebook.v1.dto;

import org.graalvm.polyglot.Context;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.Objects;

/**
 * POJO that holds context to interpret the code and result of the code.
 */
public class CodeInterpreter {
    private Context context;
    private ByteArrayOutputStream outputStream;
    private Date updatedAt;

    public CodeInterpreter(Context context, ByteArrayOutputStream outputStream, Date updatedAt) {
        this.context = context;
        this.outputStream = outputStream;
        this.updatedAt = updatedAt;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ByteArrayOutputStream getOutputStream() {
        return outputStream;
    }

    public void setOutputStream(ByteArrayOutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public String getResult() {
        String result = outputStream.toString().replace("\n", "");
        outputStream.reset();
        return result;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CodeInterpreter that = (CodeInterpreter) o;
        return Objects.equals(context, that.context) &&
                Objects.equals(outputStream, that.outputStream) &&
                Objects.equals(updatedAt, that.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(context, outputStream, updatedAt);
    }

    @Override
    public String toString() {
        return "CodeInterpreter{" +
                "context=" + context +
                ", outputStream=" + outputStream +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
