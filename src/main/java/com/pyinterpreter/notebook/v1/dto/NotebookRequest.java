package com.pyinterpreter.notebook.v1.dto;

import com.pyinterpreter.notebook.common.validation.VerifyCode;

import java.util.Objects;

/**
 * DTO that represent code sent by the client
 */
public class NotebookRequest {
    @VerifyCode
    private String code;

    private String sessionId;

    public NotebookRequest() {
    }

    public NotebookRequest(String code, String sessionId) {
        this.code = code;
        this.sessionId = sessionId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NotebookRequest that = (NotebookRequest) o;
        return Objects.equals(code, that.code) &&
                Objects.equals(sessionId, that.sessionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, sessionId);
    }

    @Override
    public String toString() {
        return "NotebookRequest{" +
                "code='" + code + '\'' +
                ", sessionId='" + sessionId + '\'' +
                '}';
    }
}
