package com.pyinterpreter.notebook.v1.dto;

import java.util.Objects;

/**
 * DTO that represent result of code
 */
public class NotebookResponse {
    private String sessionId;
    private String result;

    public NotebookResponse(String sessionId, String result) {
        this.sessionId = sessionId;
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NotebookResponse that = (NotebookResponse) o;
        return Objects.equals(sessionId, that.sessionId) &&
                Objects.equals(result, that.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionId, result);
    }

    @Override
    public String toString() {
        return "NotebookResponse{" +
                "sessionId='" + sessionId + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}
