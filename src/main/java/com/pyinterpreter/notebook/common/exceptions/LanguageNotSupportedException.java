package com.pyinterpreter.notebook.common.exceptions;

/**
 * Exception triggered when user sent a code that is not supported
 */
public class LanguageNotSupportedException extends RuntimeException {
    public LanguageNotSupportedException(String message) {
        super(message);
    }
}
