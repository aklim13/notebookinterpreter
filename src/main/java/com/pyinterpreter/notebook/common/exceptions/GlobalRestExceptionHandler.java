package com.pyinterpreter.notebook.common.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestControllerAdvice
public class GlobalRestExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger logging = LoggerFactory.getLogger(GlobalRestExceptionHandler.class);

    /**
     * return error when user send bad data requests
     *
     * @param ex
     * @param headers
     * @param status
     * @param request
     * @return error response
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logging.info("handle method arguments not valid");

        Map<String, Object> mapResponses = new HashMap<>();
        mapResponses.put("date", new Date());
        mapResponses.put("status", status);

        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .filter(elem -> !elem.isEmpty())
                .collect(Collectors.toList());

        mapResponses.put("errors", errors);
        return new ResponseEntity<>(mapResponses, headers, status);
    }

    @ExceptionHandler({CodeInterpreterException.class, LanguageNotSupportedException.class})
    @ResponseBody
    protected ResponseEntity<Object> handleParamNotValidException(Exception e,
                                                                  WebRequest request) {
        logging.info("handle parameter not valid exception");
        Map<String, Object> mapResponses = new HashMap<>();

        mapResponses.put("date", new Date());
        mapResponses.put("status", HttpStatus.BAD_REQUEST);
        mapResponses.put("code", HttpStatus.BAD_REQUEST.value());
        mapResponses.put("message", e.getMessage());

        HttpHeaders headers = new HttpHeaders();

        return handleExceptionInternal(e, mapResponses, headers, HttpStatus.BAD_REQUEST, request);
    }


}
