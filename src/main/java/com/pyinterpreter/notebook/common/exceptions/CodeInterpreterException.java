package com.pyinterpreter.notebook.common.exceptions;

/**
 * Exception triggered when Code interpreter cannot evaluate the code
 */
public class CodeInterpreterException extends RuntimeException {
    public CodeInterpreterException(String message) {
        super(message);
    }
}
