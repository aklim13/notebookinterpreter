package com.pyinterpreter.notebook.common.validation;

import com.pyinterpreter.notebook.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class VerifyCodeValidator implements ConstraintValidator<VerifyCode, String> {
    private static final Logger logging = LoggerFactory.getLogger(VerifyCodeValidator.class);
    private static String regexPattern = "(%)([^\\s]+) ";
    private static Pattern pattern = Pattern.compile(regexPattern);

    @Override
    public void initialize(VerifyCode verifyCode) {
        /* nothing to initialize here */
    }

    @Override
    public boolean isValid(String requestCode, ConstraintValidatorContext constraintValidatorContext) {
        if (requestCode == null)
            return false;

        // requestCode is null or requestCode does not have any value
        if (requestCode.isEmpty()) {
            logging.info("request code is empty");
            constraintValidatorContext.buildConstraintViolationWithTemplate("you need to add requestCode to interpret and it must respect the following format: %<lang> <requestCode>")
                    .addConstraintViolation();
            return false;
        }

        // requestCode must respect the specified format
        if (!isCodeValid(requestCode)) {
            logging.info("code doesn't respect pre defined format.");
            constraintValidatorContext.buildConstraintViolationWithTemplate("requestCode must respect the following format : %<lang-name> <requestCode>")
                    .addConstraintViolation();
            return false;
        }

        return true;
    }

    /**
     * validate code sent by the user, it should follow this format %CODE WHITESPACE CODE
     *
     * @param requestCode
     * @return true if code is valid
     */
    public boolean isCodeValid(String requestCode) {
        String language = StringUtils.extractLanguage(true, requestCode);
        String code = StringUtils.extractCode(requestCode);

        return pattern.matcher(language).matches() && !code.isEmpty();
    }
}
