package com.pyinterpreter.notebook.common.utils;

public class StringUtils {
    private StringUtils() {

    }

    /**
     * extract code from user's request
     *
     * @param requestCode user's request
     * @return code to execute
     */
    public static String extractCode(String requestCode) {
        if (requestCode.indexOf(' ') != -1) {
            return requestCode.substring(requestCode.indexOf(' '));
        }
        return "";
    }

    /**
     * extract language with % or without it
     *
     * @param withPercent remove sign % at the beginning of string
     * @param requestCode code sent by user
     * @return language (e.g. python or %python)
     */
    public static String extractLanguage(boolean withPercent, String requestCode) {
        if (requestCode.indexOf(' ') != -1) {
            if (withPercent) {
                return requestCode.substring(0, requestCode.indexOf(' ') + 1);
            } else {
                return requestCode.substring(1, requestCode.indexOf(' '));
            }
        }
        return "";
    }
}
