package com.pyinterpreter.notebook.common.configuration.beans;

import com.pyinterpreter.notebook.v1.dto.CodeInterpreter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
public class SessionBean {
    /**
     * Singleton instance that will store all session in memory.
     *
     * @return singleton concurrentHashMap
     */
    @Bean
    public Map<String, CodeInterpreter> getSessions() {
        return new ConcurrentHashMap<>();
    }
}
