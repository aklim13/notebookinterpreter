package com.pyinterpreter.notebook.common.configuration;

import com.pyinterpreter.notebook.v1.dto.CodeInterpreter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;

@Component
public class ScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

    private Map<String, CodeInterpreter> sessions;

    @Autowired
    ScheduledTasks(Map<String, CodeInterpreter> sessions) {
        this.sessions = sessions;
    }

    /**
     * Remove all contexts that were created 2 hours ago and never accessed.
     */
    @Scheduled(fixedRate = 1000 * 60 * 60) // run this each hour
    public void scheduleTaskWithFixedRate() {
        Instant now = Instant.now();
        sessions.forEach((s, codeInterpreter) -> {
            Instant lastTimeCodeInterpreterUpdated = codeInterpreter.getUpdatedAt().toInstant();

            // clear all contexts from session that were created 2 hours ago.
            if (lastTimeCodeInterpreterUpdated.isBefore(now.minus(120, ChronoUnit.HOURS))) {
                logger.debug("{} CodeInterpreter removed from sessions {}", s, codeInterpreter);
                sessions.remove(s);
            }
        });
    }
}
